
async function escalationTimer(taskcreateDateTime){
    try {
  
      const taskStartdate = Date.parse(taskcreateDateTime)
      
      console.log("Task Creation datetime in miliseconds is : "+taskStartdate)
      
      const rightnow = Date.now()
      
      console.log("Current datetime in miliseconds is : "+rightnow)

      const difference = rightnow - taskStartdate
      
      console.log("returning difference between task creation datetime and current datetime.....")
      
      return difference
      
    } catch (err) {
                    
      console.log(err.message);
      return err.message; //can be console.error
  }
      
  }
  

module.exports.escalationTimer = escalationTimer