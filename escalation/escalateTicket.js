const body = require('../spaces/prepareBody')
const postMessage = require('../spaces/postMessage')
const postTask = require('../spaces/postTask')
const appSecret = require('../vars/appSecret.conf')

async function escalateTicket(req, res, responseId, escalationLevel){

    try {
  
      
      
      if(escalationLevel == 0) {
  
        console.log("No need to escalate any tasks")

        console.log("Nothing to escalate")

        
  
      } else if (escalationLevel == 1) {
  
        console.log("task to be escalated for the first time")
        
        
        const spacesMessageBody = await body.prepareMessageBody(responseId._id, responseId.content.bodyText, responseId.content.description)
                  
        const response1 = await postMessage.postMessage(req, res, spacesMessageBody, appSecret.oncallSpacesId)
  
        console.log("type of response1 is"+typeof response1)

  
      } else if (escalationLevel == 2) {
  
                      console.log("task has been escalated a second time, writing a escalation ")                 
  
                      const spacesTaskBody = await body.prepareTaskBody(responseId.content.bodyText, responseId.content.description )
                  
                      const response2 = await postTask.postTask(req, res, spacesTaskBody, appSecret.escalationSpacesId)
  
                      console.log("escalation response is: "+ response2.data[0]._id)

                      console.log("type of response2 is "+ typeof response2)
  
                      res.send("escalation response is: "+ response2.data[0]._id)

                    return response2
      } else {
  
        console.log("something wrong , can not find escalationLevel")
  
      }
  
    } catch (err) {
      console.log(err.message); //can be console.error
    }
  }

function prepareResponse(resArr){
  resArr.forEach(function(resArr){
    return resArr._id
  })
}

module.exports.escalateTicket = escalateTicket