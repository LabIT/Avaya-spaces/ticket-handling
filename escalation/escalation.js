const spacesTask = require('../spaces/getTask')
const escalateTicket = require('./escalateTicket')
const setEscalationLevel = require('./escalationLevel')


async function escalateTask(req, res, spacesId) {
    try {

      console.log("Retrieving all tasks from space : "+spacesId)

      const response = await spacesTask.getTask(spacesId)

      const responseId = response.data

      console.log("found "+responseId.length+" number of tasks")

      
      const finalResponse = responseId.forEach(async function(responseId) 
      
      {

          console.log("calling function setEscalationLevel.setEscalationLevel() with parameter responseId : "+responseId)

          const escalationLevel =  await setEscalationLevel.setEscalationLevel(responseId)
          
          console.log("escalation level is "+ escalationLevel)

          const escalateTask = await escalateTicket.escalateTicket(req, res, responseId, escalationLevel)

          console.log("type of escalate task is "+typeof escalateTask)


      });
      
    } catch (err) {
      console.log(err.message); //can be console.error
    }
}

module.exports.escalateTask = escalateTask