const escalationCheck = require('./escalationTimer')
const appVar = require('../vars/appVar.conf')

async function setEscalationLevel(responseId){
    try {
         
      
          
          console.log("Trying to establish escalation level for task : "+responseId._id)  
          console.log("Calling function escalationCheck.escalationTimer() with task created datetime : "+responseId.created)        
  
          const difference = await escalationCheck.escalationTimer(responseId.created)
 
          console.log("difference between task creation datetime and current datetime in milliseconds is : "+difference)
          
           
  
          if(difference>appVar.firstEscalationTimer && difference<appVar.secondEscalationTimer && responseId.content.assignees.length == 0){
            taskEscalationLevel = 1
  
          } else if (difference>appVar.secondEscalationTimer && responseId.content.assignees.length == 0){
            taskEscalationLevel = 2
  
          } else {
            taskEscalationLevel = 0
  
          }
  
          console.log("task escalation level is : "+taskEscalationLevel)
          return taskEscalationLevel
  
        
         
  
  
    } catch (err) {
      console.log(err.message); //can be console.error
    }
  }


module.exports.setEscalationLevel = setEscalationLevel
  
  