# Ticket-handling

This application takes details from mail parser and updates tasks in avaya spaces. It also escalates tasks based on configured times , there are two levels of escalations configured in the app 
1. on the same spaces - escalation to peer engineer 
2. management spaces - escalation to oncall manager
