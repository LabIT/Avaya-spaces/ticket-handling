const spacesAgent = require("superagent");
const appVar = require('../vars/appVar.conf');
const appSecret = require('../vars/appSecret.conf');

async function postTask(req, res, taskBody, spacesId) {
    try {


      console.log("creating a new spaces task")

      console.log("calling spaces API : "+appVar.spacesUrl+appVar.spacesUserApi+'/tasks')

        const response = await spacesAgent
                         .post(appVar.spacesUrl+appVar.spacesApi+spacesId+'/tasks')
                         .set('Authorization', appSecret.spacesTokenType+' '+appSecret.spacesToken)
                         .set('Content-Type', appVar.contentType)
                         .send(taskBody)

                         
        console.log("Spaces connection has returned response : ", response.statusCode);
        
        console.log("returning Response Body:", response.body);
        
        return response.body
      
    } catch (err) {
      console.log(err.message); //can be console.error
    }
}

module.exports.postTask = postTask