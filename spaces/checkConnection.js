
const spacesAgent = require("superagent");
const appVar = require('../vars/appVar.conf');
const appSecret = require('../vars/appSecret.conf');

/**
 * Checks the connections to spaces
 * @returns response.statusCode 
 */
async function checkConnection() {
    try {

      console.log("Checking spaces connection....")

      console.log("calling spaces API : "+appVar.spacesUrl+appVar.spacesUserApi+'me')
      
      const response = await spacesAgent
                         .get(appVar.spacesUrl+appVar.spacesUserApi+'me')
                         .set('Authorization', appSecret.spacesTokenType+' '+appSecret.spacesToken)
      
      console.log("Spaces connection has returned response : "+response.statusCode)                   
      
      return response.statusCode
                     
    } catch (err) {
                    
                     console.log(err.message);
                     return err.message; //can be console.error
    }
}



module.exports.checkConnection = checkConnection

  