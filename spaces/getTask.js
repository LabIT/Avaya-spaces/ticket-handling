const spacesAgent = require("superagent");
const appVar = require('../vars/appVar.conf');
const appSecret = require('../vars/appSecret.conf');



/**
 * 
 * @param {spacesId} getTask
 * @returns 
 */
async function getTask(spacesId) {
    try {

      console.log("Retreiving all tasks from space : "+spacesId+"with settings , taskSizeFilter : "+appVar.taskSizeFilter+" , taskPageFilter : "+appVar.taskPageFilter+" , taskStatusFilter : "+appVar.taskStatusFilter)

      console.log("calling spaces API : "+appVar.spacesUrl+appVar.spacesUserApi+'/tasks')
  
      const response = await spacesAgent
                             .get(appVar.spacesUrl+appVar.spacesApi+spacesId+'/tasks')
                             .set('Authorization', appSecret.spacesTokenType+' '+appSecret.spacesToken)
                             .query({size: appVar.taskSizeFilter, page: appVar.taskPageFilter, status: appVar.taskStatusFilter})
      console.log("Spaces connection has returned response : ", response.statusCode);
      console.log('returning Response Body:', response.body);
  
      return response.body
  
    }catch (err) {
                    
                     console.log(err.message);
                     return err.message; //can be console.error
    }
    
  }

module.exports.getTask = getTask