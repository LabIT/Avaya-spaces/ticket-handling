async function prepareTaskBody(ticketNumber, ticketDescription) {

    console.log("function prepareTaskBody received ticketNumber : "+ticketNumber+" and description : "+ticketDescription )
    
    return TaskBody = {
      "status":0,
      "content":
        {
         "bodyText": ticketNumber,
         "description": ticketDescription,
         "assignees":[],
         "dueDate":"",
         "status":"pending"
        }
      }  
}



async function prepareMessageBody(ticketNumber, ticketDescription){


  console.log("function prepareMessageBody received ticketNumber : "+ticketNumber+" and description : "+ticketDescription )
  
  return MessageBody = {
    "content":
     {"bodyText": "There has been no action on incident"+ticketNumber+"and description"+ticketDescription+"for last 5 minuts, it will be escalated if there is no response in next 5 minutes",
     "data":[]
    }
  }

}

module.exports.prepareTaskBody = prepareTaskBody
module.exports.prepareMessageBody = prepareMessageBody