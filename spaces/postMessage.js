const spacesAgent = require("superagent");
const appVar = require('../vars/appVar.conf');
const appSecret = require('../vars/appSecret.conf');

async function postMessage(req, res, spacesMessageBody, spacesId) {
    try {

      console.log("creating a new spaces message : ")
      console.log("calling spaces API : "+appVar.spacesUrl+appVar.spacesApi+spacesId+'/chats')

      const response = await spacesAgent
                              .post(`${appVar.spacesUrl + appVar.spacesApi + spacesId}/chats`)
                              .set('Authorization', appSecret.spacesTokenType+' '+appSecret.spacesToken)
                              .set('Content-Type', 'application/json')
                              .send(spacesMessageBody)
        
      console.log("Spaces connection has returned response : ", response.statusCode);
      console.log('returning Response Body:', response.body);
          
      return response.body

    } catch (err) {
      console.log(err.message); //can be console.error
    }
}

module.exports.postMessage = postMessage
