/**
 * Gss Alert App
 */

//------------------ Modules-----------------------------/
const express = require("express");               


const appVar = require('./vars/appVar.conf');
const appSecret = require('./vars/appSecret.conf');

const spacesConnection = require('./spaces/checkConnection')


const spacesTaskPost = require('./spaces/postTask');

const spacesTaskGet = require('./spaces/getTask');
const spacesMessagePost = require('./spaces/postMessage');

const body = require('./spaces/prepareBody');

const escalation = require('./escalation/escalation')


//------------------ App Setup---------------------------/
const app = express();




// Start the server
const PORT = process.env.PORT || appVar.serverPort;
app.listen(PORT, () => {console.log(`App listening on port ${PORT}`);
});


app.use(express.json());




//-------------------App Route----------------------------/


/**
 * App route :  Default
 */

app.get('/', (req, res) => {res.status(200).send('Who is on call?').end();
});



/**
 * App route : /test
 * Validates connections to spaces 
 * @params none 
 * @returns response.statusCode
 */

app.get('/test', (req, res) => {

    console.log("route : /test has been invoked");

    (async() => {

      console.log("calling function spacesConnection.checkConnection()...")  

      const response = await spacesConnection.checkConnection()
      
      console.log("responding with connection status...."+response)

      res.send(" Response status : "+response)

    })(); 
});
  

/**
 * App route : /hook
 * Posts a task in spaces and returns the Id of the task
 * @params sr_number, sr_number
 * @params spacesId
 * @returns response.data[0]._id
 * 
 */

app.post("/hook",(req, res, next) => {

    console.log("route : /hook has been invoked");

    (async() => { 

        console.log("calling function spacesConnection.checkConnection()")
        if(await spacesConnection.checkConnection() == 200){

            console.log("spaces connection check has been successful.")
            console.log("calling function body.prepareTaskBody to prepare spaces task body with ticket number : "+req.body.sr_number+" and description "+req.body.sr_desc)
        
            const TaskBody = await body.prepareTaskBody(req.body.sr_number, req.body.sr_desc)
            
            console.log("Received TaskBody : ", TaskBody)
            console.log("calling function spacesTaskPost.postTask to create a new task in space : "+appSecret.oncallSpacesId) 

            const response = await spacesTaskPost.postTask(req, res, TaskBody, appSecret.oncallSpacesId)

            console.log("Task created successfully with ID: "+ response.data[0]._id )
            
            res.send("Task created successfully with ID: "+ response.data[0]._id )

        } else {
            res.send("Connection status: " + await spacesConnection.checkConnection())
        }

        
      })(); 
});


/**
 * App route : /gettask
 * Retrieves tasks from a space
 * @params spacesId
 * @returns response.data
 */


app.get("/gettask",(req, res, next) => {

    console.log("route : /gettask has been invoked");

    (async() => { 

        if(await spacesConnection.checkConnection() == 200){

            console.log("spaces connection check has been successful.")
            console.log("calling function spacesTaskGet.getTask with parameter appSecret.oncallSpacesId : "+appSecret.oncallSpacesId)

            const response = await spacesTaskGet.getTask(appSecret.oncallSpacesId)

            console.log("received the following response from spacesTaskGet.getTask() : ")
            console.log(response.data)

            res.send(response.data)

        } else {
            res.send("Connection status: " + await spacesConnection.checkConnection())
        }

        
      })(); 
});


/**
 * App route : /postmessage
 * Posts a message on spaces
 * @params sr_number, sr_desc
 * @params spacesId
 * @returns response.data
 */

app.post("/postmessage",(req, res, next) => {

    console.log("route : /postmessage has been invoked");

    (async() => { 

        if(await spacesConnection.checkConnection() == 200){

            console.log("spaces connection check has been successful.")
            console.log("calling function body.prepareMessageBody with parameters :") 
            console.log("sr_number : "+req.body.sr_number)
            console.log(" and task description : "+req.body.sr_desc)

            const spacesMessageBody = await body.prepareMessageBody(req.body.sr_number, req.body.sr_desc)

            console.log("calling function spacesMessagePost.postMessage with messagebody : "+spacesMessageBody+" and spacesID : "+appSecret.oncallSpacesId)
                
            const response = await spacesMessagePost.postMessage(req, res, spacesMessageBody, appSecret.oncallSpacesId)

            console.log("recevied the following response from spacesMessagePost.postMessage : ")
            console.log(response.data)

            res.send(response.data)

        } else {
            res.send("Connection status: " + await spacesConnection.checkConnection())
        }

        
      })(); 
});


/**
 * App route : /escalate
 * Escalates tasks in spaces based on defined criteria
 * @params spacesId
 * @returns none -----> needs to be fixed
 */


app.post("/escalate",(req, res, next) => {

    console.log("route : /escalate has been invoked");

    
    (async() => { 

        if(await spacesConnection.checkConnection() == 200){
        
            console.log("spaces connection check has been successful.")
            console.log("calling function escalation.escalateTask() with spacesId :"+appSecret.oncallSpacesId) 
           
            const response = await escalation.escalateTask(req, res, appSecret.oncallSpacesId)
        
            
            res.send("Any tasks that need escalation have been escalated, please check logs for more details")
         
        } else {
            res.send("Connection status: " + await spacesConnection.checkConnection())
        }

        
      })(); 
});


